class Fighter {
    constructor(fighter) {
        this.name = fighter.name;
        this.health = fighter.health;
        this.attack = fighter.attack;
        this.defense = fighter.defense;
    }

    getHitPower() {
        const criticalHitChance = Math.random() + 1;
        const power = Math.round(criticalHitChance * this.attack);
        return power;
    }

    getBlockPower() {
        const dodgeChance = Math.random() + 1;
        const power = Math.round(dodgeChance * this.defense);
        return power;
    }
}

export default Fighter;